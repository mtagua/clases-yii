<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<button class="btn btn-lg btn-warning"><span class=" glyphicon glyphicon-off">   PHP</span></button>
<div class="site-index">

    <div class="jumbotron">
        <h1>Muy bien Mauri!</h1>

        <p class="lead">Ha creado con éxito su potente aplicación Yii 2.0.</p>

        <button class="btn btn-danger  glyphicon glyphicon-glass ">BUTTON</button>
        


        <p><a class="btn btn-lg btn-success" href="http://www.inv.gov.ar" target="_blank">IR A LA PAGINA DEL INV</a></p>
    </div>
    <button class=" btn btn-success btn-sm ">NUEVO</button>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>IMPORTANTE</h2>
                <button class=" btn  btn-default btn-xs"><span class="glyphicon glyphicon glyphicon-remove"></span>CANCELAR</button>
                <button class=" btn btn-success "><span class="glyphicon glyphicon-ok"></span>ACEPTAR</button>
              
                

            
            </div>
            <div class="col-lg-4">
                <h2>IMPORTANTE</h2>
                <button class=" btn btn-primary"><span class="glyphicon glyphicon-star"> ESTRELLA</span></button>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-success" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>IMPORTANTE</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-info" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
